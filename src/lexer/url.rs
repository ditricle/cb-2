use logos::{Lexer, Logos, Source};
use std::fmt::{Display, Formatter};

/// Tuple struct for link URLs
#[derive(Debug, PartialEq)]
pub struct LinkUrl(String);

/// Implement Display for printing
impl Display for LinkUrl {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

/// Tuple struct for link texts
#[derive(Debug, PartialEq)]
pub struct LinkText(String);

/// Implement Display for printing
impl Display for LinkText {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

/// Token enum for capturing of link URLs and Texts
#[derive(Logos, Debug, PartialEq)]
pub enum URLToken {
    // TODO: Capture link definitions
    //wasnt able to look for href="[^"]" even with escape char, so href= must do the trick
    #[regex(r"<a[^>]+href=[^>]*>[^<]*</a(\s)*>", extract_link_info)]//<a[^>]*>[^<]*</a[^>]*>
    Link((LinkUrl, LinkText)),

    // TODO: Ignore all characters that do not belong to a link definition
    //#[regex("", logos::skip)] //<[^a][^*]*>[^(<a)]*
    #[regex("((<[^a])|[^<])*", logos::skip)]
    //this works because it has a lower priority i guess...
    #[regex("<a[^>]*>", logos::skip)]
    Ignored,

    // Catch any error
    #[error]
    Error,
}

/// Extracts the URL and text from a string that matched a Link token
fn extract_link_info(lex: &mut Lexer<URLToken>) -> (LinkUrl, LinkText) {
    // TODO: Implement extraction from link definition
    //println!("{}", lex.slice());
    let mut s = String::from(lex.slice());
    
    // this is sooo dirty, but i cant code in rust... :/
    let pos = s.find("href=");
    match pos {
        None => (),
        Some(p) => s = s[p+6..].to_string(),
    }
    let pos = s.find("\"");
    let mut url = String::from("");
    match pos {
        None => (),
        Some(p) => url.push_str(&s[..p].to_string()),
    }
    let pos = s.find(">");
    match pos {
        None => (),
        Some(p) => s = s[p+1..].to_string(),
    }
    let pos = s.find("<");
    let mut text = String::from("");
    match pos {
        None => (),
        Some(p) => text.push_str(&s[..p].to_string()),
    }
    //println!("{}", s);
    //println!("{}", url);
    //println!("{}", text);

    (LinkUrl(String::from(url)),LinkText(String::from(text)))
}
